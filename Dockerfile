FROM debian:stable
RUN apt-get update && apt-get install -y nginx-full libnginx-mod-rtmp python3-flask python3-gunicorn python3-pip stunnel ca-certificates
RUN pip3 install dataset
RUN mkdir /flask-app
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stdout /var/log/nginx/error.log
RUN rm /etc/nginx/sites-enabled/default
RUN gunzip /usr/share/doc/libnginx-mod-rtmp/examples/stat.xsl.gz
COPY app.py /flask-app/app.py
COPY static /flask-app/static
COPY templates /flask-app/templates
COPY confs/rtmp.conf /etc/nginx/modules-enabled/99-rtmp-additional-configuration.conf
COPY confs/http.conf /etc/nginx/sites-enabled/
COPY confs/stunnel.conf /etc/stunnel/stunnel.conf
VOLUME /nginx-configs
WORKDIR /flask-app
CMD flask run --debugger --host 0.0.0.0
