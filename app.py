from flask import Flask, request, jsonify, render_template
import json

app = Flask(__name__)
app.config["JSON_DB"] = "/nginx-configs/restreamer.json"
app.config['TEMPLATES_AUTO_RELOAD'] = True


def current_streams():
    with open(app.config["JSON_DB"]) as f:
        return json.load(f).get('output_streams', [])


def write_config():
    template = app.jinja_env.get_template('nginx.conf')
    streams = current_streams()
    with open('/nginx-configs/rtmp-restreams.conf', 'w') as f:
        f.write(template.render(streams=streams))
    # TODO: reload nginx


@app.route("/")
def index():
    return render_template('index.html')


@app.route('/api/output_streams', methods=["PUT", "GET"])
def streams():
    if request.method == "PUT":
        with open(app.config["JSON_DB"], 'w') as f:
            json.dump({"output_streams": request.json}, f)
        write_config()
    return jsonify(current_streams())


if __name__ == "__main__":
    app.run(debug=True)
