var PREFIXES = {
  youtube: "rtmp://a.rtmp.youtube.com/live2/",
  twitch: "rtmp://live.twitch.tv/app/",
  periscope: "rtmp://or.pscp.tv:80/x/",
  facebook: "rtmp://stunnel:19350/rtmp/"
}

function xmlToDict(xml) {
  var out = {};
  for(var i = 0; i < xml.children.length; i++) {
    if(xml.children[i].childElementCount == 0) {
      out[xml.children[i].tagName] = xml.children[i].textContent;
    }
  }
  return out;
}

function getInputStreams(xml) {
  var stream_tags = xml.getElementsByTagName('stream');
  var streams = [];
  for(var i = 0; i < stream_tags.length; i++) {
    streams.push(xmlToDict(stream_tags[i]));
  }
  return streams;
}

var vm = new Vue({
  el: "#wrapper",
  data: {
    keys: {
      youtube: '',
      twitch: '',
      periscope: '',
      facebook: ''
    },
    rtmp_stat: {},
    add_url: '',
    input_streams: [],
    custom_output_streams: [],
  },
  methods: {
    remove_stream: function(e) {
      this.custom_output_streams.splice(e.target.dataset.index, 1);
    },
    update_stat: function() {
      var xhr = new XMLHttpRequest;
      xhr.open('GET', '/stat');
      xhr.onload = function () {
        if(xhr.readyState === xhr.DONE && xhr.status === 200) {
          var input_streams = getInputStreams(xhr.responseXML);
          vm.input_streams = input_streams;
          var rtmp_stat = xmlToDict(xhr.responseXML.children[0]);
          vm.rtmp_stat = rtmp_stat;
        }
      };
      xhr.send();
    },
    add_custom_output: function() {
      this.custom_output_streams.push("");
    },
    save: function(e) {
      var originalText = e.target.innerText;
      e.target.innerText = "...";
      var urls = [];
      Object.keys(this.keys).forEach((key) => {
        if(this.keys[key] != "") {
          urls.push(PREFIXES[key] + this.keys[key]);
        }
      });
      this.custom_output_streams.forEach((s) => {
        if(urls.indexOf(s) === -1) {
          urls.push(s)
        }
      });
      fetch('/api/output_streams', {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(urls)
      }).then(this.update_ouput_streams).then(() => {
        e.target.innerText = originalText;
      })
    },
    update_ouput_streams: function(response) {
      return response.json().then(r => {
        this.output_streams = r;
        r.forEach((url) => {
          var added = false;
          Object.keys(PREFIXES).forEach((service) => {
            if(url.startsWith(PREFIXES[service])) {
              this.keys[service] = url.replace(PREFIXES[service], "");
              added = true;
            }
          });
          if(!added) {
            if(this.custom_output_streams.indexOf(url) == -1) {
              this.custom_output_streams.push(url);
            }
          }
        })
      })
    }
  }
});

fetch('/api/output_streams').then(vm.update_ouput_streams);
document.querySelectorAll("#noscript").forEach(e => e.style.display = "none");
document.querySelector('#wrapper').style.display = "flex";
setInterval(vm.update_stat, 500);
